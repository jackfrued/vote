import hashlib
import random
import re

import requests

ALL_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'


def gen_md5_digest(content):
    """将字符串处理成MD5摘要"""
    return hashlib.md5(content.encode()).hexdigest()


def gen_sha256_digest(content):
    """将字符串处理成SHA-256摘要"""
    return hashlib.sha256(content.encode()).hexdigest()


def gen_random_code(length=4):
    """生成指定长度的随机验证码"""
    return ''.join(random.choices(ALL_CHARS, k=length))


def gen_mobile_code(length=6):
    """生成指定长度的手机验证码"""
    return ''.join(random.choices('0123456789', k=length))


TEL_PATTERN = re.compile(r'1[3-9]\d{9}')


def check_tel(tel):
    """检查手机号是否有效"""
    return TEL_PATTERN.fullmatch(tel) is not None


# 项目中无法自行完成的功能，可以借助三方平台来实现，接入三方平台基本上就两种方式：
# 1. API接入（调用三方的数据接口获得服务） ---> 短信、地图、实名认证、企业认证、天气、路况
# 2. SDK接入（安装三方库调用其中的类、方法、函数）---> 支付、云存储
def send_message_by_sms(tel, message):
    resp = requests.post(
        url='http://sms-api.luosimao.com/v1/send.json',
        auth=('api', 'key-850e8bf271c17d1e0fe12a7754807f32'),
        data={
            'mobile': tel,
            'message': message
        },
        timeout=5,
        verify=False
    )
    return resp.json()


# result = send_message_by_sms(
#     '13548041193',
#     '您的短信验证码是111222，打死也不能告诉别人。【Python小课】'
# )
# print(result)
